#pragma once

#include "engine/render/renderer.h"
#include "engine/render/vbo.h"
#include "cube.h"

/**
  * On utilise des chunks pour que si on modifie juste un cube, on ait pas
  * besoin de recharger toute la carte dans le buffer, mais juste le chunk en question
  */
class MChunk
{
	public :

		static const int CHUNK_SIZE = 64; ///< Taille d'un chunk en nombre de cubes (n*n*n)
		MCube _Cubes[CHUNK_SIZE][CHUNK_SIZE][CHUNK_SIZE]; ///< Cubes contenus dans le chunk

		YVbo * VboOpaque = NULL;
		YVbo * VboTransparent = NULL;

		MChunk * Voisins[6];

		int _XPos, _YPos, _ZPos; ///< Position du chunk dans le monde

		MChunk(int x, int y, int z)
		{
			memset(Voisins, 0x00, sizeof(void*)* 6);
			_XPos = x;
			_YPos = y;
			_ZPos = z;
		}

		/*
		Creation des VBO
		*/

		//On met le chunk dans son VBO
		void toVbos(void)
		{
			SAFEDELETE(VboOpaque);
			SAFEDELETE(VboTransparent);

			//Compter les sommets
			int* nbVertOpaque = new int();
			int* nbVertTransp = new int();
			foreachVisibleTriangle(true, nbVertOpaque, nbVertTransp, nullptr, nullptr);

			//Cr�er les VBO
			VboOpaque = new YVbo(4, *nbVertOpaque, YVbo::PACK_BY_ELEMENT_TYPE);

			VboOpaque->setElementDescription(0, YVbo::Element(3)); //Sommet
			VboOpaque->setElementDescription(1, YVbo::Element(3)); //Normale
			VboOpaque->setElementDescription(2, YVbo::Element(2)); //UV
			VboOpaque->setElementDescription(3, YVbo::Element(1)); //Type
			VboOpaque->createVboCpu();

			VboTransparent = new YVbo(4, *nbVertTransp, YVbo::PACK_BY_ELEMENT_TYPE);
			VboTransparent->setElementDescription(0, YVbo::Element(3)); //Sommet
			VboTransparent->setElementDescription(1, YVbo::Element(3)); //Normale
			VboTransparent->setElementDescription(2, YVbo::Element(2)); //UV
			VboTransparent->setElementDescription(3, YVbo::Element(1)); //Type
			VboTransparent->createVboCpu();

			//Remplir les VBO
			foreachVisibleTriangle(false, nbVertOpaque, nbVertTransp, VboOpaque, VboTransparent);
			
			VboOpaque->createVboGpu();
			VboTransparent->createVboGpu();
		}

		//Ajoute un quad du cube. Attention CCW
		int addQuadToVbo(YVbo * vbo, int iVertice, const YVec3f & a, const YVec3f & b, const YVec3f & c, const YVec3f & d, float type) 
		{
			YVec3f normal = (b - a).cross(d - a);
			normal.normalize();

			vbo->setElementValue(0, iVertice, a.X, a.Y, a.Z);
			vbo->setElementValue(1, iVertice, normal.X, normal.Y, normal.Z);
			vbo->setElementValue(2, iVertice, 0, 0);
			vbo->setElementValue(3, iVertice, type);
								
			iVertice = iVertice+1;		
								
			vbo->setElementValue(0, iVertice, b.X, b.Y, b.Z);
			vbo->setElementValue(1, iVertice, normal.X, normal.Y, normal.Z);
			vbo->setElementValue(2, iVertice, 1, 0);
			vbo->setElementValue(3, iVertice, type);
								
			iVertice = iVertice + 1;
								
			vbo->setElementValue(0, iVertice, c.X, c.Y, c.Z);
			vbo->setElementValue(1, iVertice, normal.X, normal.Y, normal.Z);
			vbo->setElementValue(2, iVertice, 1, 1);
			vbo->setElementValue(3, iVertice, type);
								
			iVertice = iVertice + 1;
								
			vbo->setElementValue(0, iVertice, a.X, a.Y, a.Z);
			vbo->setElementValue(1, iVertice, normal.X, normal.Y, normal.Z);
			vbo->setElementValue(2, iVertice, 0, 0);
			vbo->setElementValue(3, iVertice, type);
								   
			iVertice = iVertice + 1;
								   
			vbo->setElementValue(0, iVertice, c.X, c.Y, c.Z);
			vbo->setElementValue(1, iVertice, normal.X, normal.Y, normal.Z);
			vbo->setElementValue(2, iVertice, 1, 1);
			vbo->setElementValue(3, iVertice, type);
								   
			iVertice = iVertice + 1;
								   
			vbo->setElementValue(0, iVertice, d.X, d.Y, d.Z);
			vbo->setElementValue(1, iVertice, normal.X, normal.Y, normal.Z);
			vbo->setElementValue(2, iVertice, 0, 1);
			vbo->setElementValue(3, iVertice, type);

			iVertice = iVertice + 1;

			return 6;
		}

		//Permet de compter les triangles ou des les ajouter aux VBO
		void foreachVisibleTriangle(bool countOnly, int * nbVertOpaque, int * nbVertTransp, YVbo * VboOpaque, YVbo * VboTrasparent) {

			YVec3f A = { 0,0,0 };
			YVec3f B = { MCube::CUBE_SIZE,0,0 };
			YVec3f C = { MCube::CUBE_SIZE, MCube::CUBE_SIZE,0 };
			YVec3f D = { 0, MCube::CUBE_SIZE,0 };
			YVec3f E = { 0,0, MCube::CUBE_SIZE };
			YVec3f F = { MCube::CUBE_SIZE,0, MCube::CUBE_SIZE };
			YVec3f G = { MCube::CUBE_SIZE, MCube::CUBE_SIZE, MCube::CUBE_SIZE };
			YVec3f H = { 0, MCube::CUBE_SIZE, MCube::CUBE_SIZE };

			*nbVertOpaque = 0;
			*nbVertTransp = 0;

			MCube** cubeXPrev = new MCube * (); //g
			MCube** cubeXNext = new MCube * (); //d
			MCube** cubeYPrev = new MCube * (); //derriere
			MCube** cubeYNext = new MCube * (); //devant
			MCube** cubeZPrev = new MCube * (); //bas
			MCube** cubeZNext = new MCube * (); //haut

			for (int x = 0; x < CHUNK_SIZE; x++)
				for (int y = 0; y < CHUNK_SIZE; y++)
					for (int z = 0; z < CHUNK_SIZE; z++)
					{
						if (!_Cubes[x][y][z].getDraw() || _Cubes[x][y][z].getType() == MCube::CUBE_AIR)
							continue;

						YVec3f O = {(float)(x+ _XPos*CHUNK_SIZE)*MCube::CUBE_SIZE , (float)(y + _YPos * CHUNK_SIZE) * MCube::CUBE_SIZE, (float)(z + _ZPos * CHUNK_SIZE) * MCube::CUBE_SIZE };

						get_surrounding_cubes(x,y,z, cubeXPrev, cubeXNext, cubeYPrev, cubeYNext, cubeZPrev, cubeZNext);

						float type = _Cubes[x][y][z].getType();
						if (_Cubes[x][y][z].isTransparent()) {
							if (*cubeXPrev != NULL) {
								if (!(*cubeXPrev)->getDraw() || (*cubeXPrev)->getType() == MCube::CUBE_AIR)
									*nbVertTransp += (countOnly) ? 6 : addQuadToVbo(VboTrasparent, *nbVertTransp, D + O, A + O, E + O, H + O, type);
							}
							else {
								*nbVertTransp += (countOnly) ? 6 : addQuadToVbo(VboTrasparent, *nbVertTransp, D + O, A + O, E + O, H + O, type);
							}
							if (*cubeXNext != NULL) {
								if (!(*cubeXNext)->getDraw() || (*cubeXNext)->getType() == MCube::CUBE_AIR)
									*nbVertTransp += (countOnly) ? 6 : addQuadToVbo(VboTrasparent, *nbVertTransp, B + O, C + O, G + O, F + O, type);
							}
							else {
								*nbVertTransp += (countOnly) ? 6 : addQuadToVbo(VboTrasparent, *nbVertTransp, B + O, C + O, G + O, F + O, type);
							}
							if (*cubeYPrev != NULL) {
								if (!(*cubeYPrev)->getDraw() || (*cubeYPrev)->getType() == MCube::CUBE_AIR)
									*nbVertTransp += (countOnly) ? 6 : addQuadToVbo(VboTrasparent, *nbVertTransp, A + O, B + O, F + O, E + O, type);
							}
							else {
								*nbVertTransp += (countOnly) ? 6 : addQuadToVbo(VboTrasparent, *nbVertTransp, A + O, B + O, F + O, E + O, type);
							}
							if (*cubeYNext != NULL) {
								if (!(*cubeYNext)->getDraw() || (*cubeYNext)->getType() == MCube::CUBE_AIR)
									*nbVertTransp += (countOnly) ? 6 : addQuadToVbo(VboTrasparent, *nbVertTransp, C + O, D + O, H + O, G + O, type);
							}
							else {
								*nbVertTransp += (countOnly) ? 6 : addQuadToVbo(VboTrasparent, *nbVertTransp, C + O, D + O, H + O, G + O, type);
							}
							if (*cubeZPrev != NULL) {
								if (!(*cubeZPrev)->getDraw() || (*cubeZPrev)->getType() == MCube::CUBE_AIR)
									*nbVertTransp += (countOnly) ? 6 : addQuadToVbo(VboTrasparent, *nbVertTransp, B + O, A + O, D + O, C + O, type);
							}
							else {
								*nbVertTransp += (countOnly) ? 6 : addQuadToVbo(VboTrasparent, *nbVertTransp, B + O, A + O, D + O, C + O, type);
							}
							if (*cubeZNext != NULL) {
								if (!(*cubeZNext)->getDraw() || (*cubeZNext)->getType() == MCube::CUBE_AIR)
									*nbVertTransp += (countOnly) ? 6 : addQuadToVbo(VboTrasparent, *nbVertTransp, E + O, F + O, G + O, H + O, type);
							}
							else {
								*nbVertTransp += (countOnly) ? 6 : addQuadToVbo(VboTrasparent, *nbVertTransp, E + O, F + O, G + O, H + O, type);
							}
						}																	
						else if (_Cubes[x][y][z].isOpaque()) {
							if (*cubeXPrev != NULL) {
								if (!(*cubeXPrev)->getDraw() || (*cubeXPrev)->isTransparent())
									*nbVertOpaque += (countOnly) ? 6 : addQuadToVbo(VboOpaque, *nbVertOpaque, D + O, A + O, E + O, H + O, type);
							}
							else {
								*nbVertOpaque += (countOnly) ? 6 : addQuadToVbo(VboOpaque, *nbVertOpaque, D + O, A + O, E + O, H + O, type);
							}
							if (*cubeXNext != NULL) {
								if (!(*cubeXNext)->getDraw() || (*cubeXNext)->isTransparent())
									*nbVertOpaque += (countOnly) ? 6 : addQuadToVbo(VboOpaque, *nbVertOpaque, B + O, C + O, G + O, F + O, type);
							}
							else {
								*nbVertOpaque += (countOnly) ? 6 : addQuadToVbo(VboOpaque, *nbVertOpaque, B + O, C + O, G + O, F + O, type);
							}
							if (*cubeYPrev != NULL) {
								if (!(*cubeYPrev)->getDraw() || (*cubeYPrev)->isTransparent())
									*nbVertOpaque += (countOnly) ? 6 : addQuadToVbo(VboOpaque, *nbVertOpaque, A + O, B + O, F + O, E + O, type);
							}
							else {
								*nbVertOpaque += (countOnly) ? 6 : addQuadToVbo(VboOpaque, *nbVertOpaque, A + O, B + O, F + O, E + O, type);
							}
							if (*cubeYNext != NULL) {
								if (!(*cubeYNext)->getDraw() || (*cubeYNext)->isTransparent())
									*nbVertOpaque += (countOnly) ? 6 : addQuadToVbo(VboOpaque, *nbVertOpaque, C + O, D + O, H + O, G + O, type);
							}
							else {
								*nbVertOpaque += (countOnly) ? 6 : addQuadToVbo(VboOpaque, *nbVertOpaque, C + O, D + O, H + O, G + O, type);
							}
							if (*cubeZPrev != NULL) {
								if (!(*cubeZPrev)->getDraw() || (*cubeZPrev)->isTransparent())
									*nbVertOpaque += (countOnly) ? 6 : addQuadToVbo(VboOpaque, *nbVertOpaque, B + O, A + O, D + O, C + O, type);
							}
							else {
								*nbVertOpaque += (countOnly) ? 6 : addQuadToVbo(VboOpaque, *nbVertOpaque, B + O, A + O, D + O, C + O, type);
							}
							if (*cubeZNext != NULL) {
								if (!(*cubeZNext)->getDraw() || (*cubeZNext)->isTransparent())
									*nbVertOpaque += (countOnly) ? 6 : addQuadToVbo(VboOpaque, *nbVertOpaque, E + O, F + O, G + O, H + O, type);
							}
							else {
								*nbVertOpaque += (countOnly) ? 6 : addQuadToVbo(VboOpaque, *nbVertOpaque, E + O, F + O, G + O, H + O, type);
							}
						}
					}
		}

		/*
		Gestion du chunk
		*/

		void reset(void)
		{
			for(int x=0;x<CHUNK_SIZE;x++)
				for(int y=0;y<CHUNK_SIZE;y++)
					for(int z=0;z<CHUNK_SIZE;z++)
					{
						_Cubes[x][y][z].setDraw(false);
						_Cubes[x][y][z].setType(MCube::CUBE_AIR);
					}
		}

		void setVoisins(MChunk * xprev, MChunk * xnext, MChunk * yprev, MChunk * ynext, MChunk * zprev, MChunk * znext)
		{
			Voisins[0] = xprev;
			Voisins[1] = xnext;
			Voisins[2] = yprev;
			Voisins[3] = ynext;
			Voisins[4] = zprev;
			Voisins[5] = znext;
		}

		void get_surrounding_cubes(int x, int y, int z, MCube ** cubeXPrev, MCube ** cubeXNext,
			MCube ** cubeYPrev, MCube ** cubeYNext,
			MCube ** cubeZPrev, MCube ** cubeZNext)
		{

			*cubeXPrev = NULL;
			*cubeXNext = NULL;
			*cubeYPrev = NULL;
			*cubeYNext = NULL;
			*cubeZPrev = NULL;
			*cubeZNext = NULL;

			if (x == 0 && Voisins[0] != NULL)
				*cubeXPrev = &(Voisins[0]->_Cubes[CHUNK_SIZE - 1][y][z]);
			else if (x > 0)
				*cubeXPrev = &(_Cubes[x - 1][y][z]);

			if (x == CHUNK_SIZE - 1 && Voisins[1] != NULL)
				*cubeXNext = &(Voisins[1]->_Cubes[0][y][z]);
			else if (x < CHUNK_SIZE - 1)
				*cubeXNext = &(_Cubes[x + 1][y][z]);

			if (y == 0 && Voisins[2] != NULL)
				*cubeYPrev = &(Voisins[2]->_Cubes[x][CHUNK_SIZE - 1][z]);
			else if (y > 0)
				*cubeYPrev = &(_Cubes[x][y - 1][z]);

			if (y == CHUNK_SIZE - 1 && Voisins[3] != NULL)
				*cubeYNext = &(Voisins[3]->_Cubes[x][0][z]);
			else if (y < CHUNK_SIZE - 1)
				*cubeYNext = &(_Cubes[x][y + 1][z]);

			if (z == 0 && Voisins[4] != NULL)
				*cubeZPrev = &(Voisins[4]->_Cubes[x][y][CHUNK_SIZE - 1]);
			else if (z > 0)
				*cubeZPrev = &(_Cubes[x][y][z - 1]);

			if (z == CHUNK_SIZE - 1 && Voisins[5] != NULL)
				*cubeZNext = &(Voisins[5]->_Cubes[x][y][0]);
			else if (z < CHUNK_SIZE - 1)
				*cubeZNext = &(_Cubes[x][y][z + 1]);
		}

		void render(bool transparent)
		{
			if (transparent)
				VboTransparent->render();
			else
				VboOpaque->render();
		}

		/**
		  * On verifie si le cube peut �tre vu
		  */
		bool test_hidden(int x, int y, int z)
		{
			MCube * cubeXPrev = NULL; 
			MCube * cubeXNext = NULL; 
			MCube * cubeYPrev = NULL; 
			MCube * cubeYNext = NULL; 
			MCube * cubeZPrev = NULL; 
			MCube * cubeZNext = NULL; 

			if(x == 0 && Voisins[0] != NULL)
				cubeXPrev = &(Voisins[0]->_Cubes[CHUNK_SIZE-1][y][z]);
			else if(x > 0)
				cubeXPrev = &(_Cubes[x-1][y][z]);

			if(x == CHUNK_SIZE-1 && Voisins[1] != NULL)
				cubeXNext = &(Voisins[1]->_Cubes[0][y][z]);
			else if(x < CHUNK_SIZE-1)
				cubeXNext = &(_Cubes[x+1][y][z]);

			if(y == 0 && Voisins[2] != NULL)
				cubeYPrev = &(Voisins[2]->_Cubes[x][CHUNK_SIZE-1][z]);
			else if(y > 0)
				cubeYPrev = &(_Cubes[x][y-1][z]);

			if(y == CHUNK_SIZE-1 && Voisins[3] != NULL)
				cubeYNext = &(Voisins[3]->_Cubes[x][0][z]);
			else if(y < CHUNK_SIZE-1)
				cubeYNext = &(_Cubes[x][y+1][z]);

			if(z == 0 && Voisins[4] != NULL)
				cubeZPrev = &(Voisins[4]->_Cubes[x][y][CHUNK_SIZE-1]);
			else if(z > 0)
				cubeZPrev = &(_Cubes[x][y][z-1]);

			if(z == CHUNK_SIZE-1 && Voisins[5] != NULL)
				cubeZNext = &(Voisins[5]->_Cubes[x][y][0]);
			else if(z < CHUNK_SIZE-1)
				cubeZNext = &(_Cubes[x][y][z+1]);

			if( cubeXPrev == NULL || cubeXNext == NULL ||
				cubeYPrev == NULL || cubeYNext == NULL ||
				cubeZPrev == NULL || cubeZNext == NULL )
				return false;

			if (cubeXPrev->isOpaque() == true && //droite
				cubeXNext->isOpaque() == true && //gauche
				cubeYPrev->isOpaque() == true && //haut
				cubeYNext->isOpaque() == true && //bas
				cubeZPrev->isOpaque() == true && //devant
				cubeZNext->isOpaque() == true)  //derriere
				return true;
			return false;
		}

		void disableHiddenCubes(void)
		{
			for(int x=0;x<CHUNK_SIZE;x++)
				for(int y=0;y<CHUNK_SIZE;y++)
					for(int z=0;z<CHUNK_SIZE;z++)
					{
						_Cubes[x][y][z].setDraw(true);
						if(test_hidden(x,y,z))
							_Cubes[x][y][z].setDraw(false);
					}
		}


};