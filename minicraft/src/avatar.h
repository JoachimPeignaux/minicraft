#ifndef __AVATAR__
#define __AVATAR__

#include "engine/utils/types_3d.h"
#include "engine/render/camera.h"
#include "engine/utils/timer.h"
#include "world.h"

class MAvatar
{
public:

	//custom consts
	const float gravity = -9.81f;
	const float vitesse = 15;

	YVec3f Position;
	YVec3f Speed;

	bool Move;
	bool Jump;
	float Height;
	float CurrentHeight;
	float Width;
	bool avance;
	bool recule;
	bool gauche;
	bool droite;
	bool Standing;
	bool InWater;
	bool Crouch;
	bool Run;
	bool Grounded;

	YCamera * Cam;
	MWorld * World;

	YTimer _TimerStanding;

	MAvatar(YCamera * cam, MWorld * world)
	{
		Position = YVec3f((MWorld::MAT_SIZE_METERS) / 2, (MWorld::MAT_SIZE_METERS) / 2, (MWorld::MAT_HEIGHT_METERS));
		Height = 1.8f;
		CurrentHeight = Height;
		Width = 0.3f;
		Cam = cam;
		Cam->moveTo(Position);
		avance = false;
		recule = false;
		gauche = false;
		droite = false;
		Standing = false;
		Jump = false;
		World = world;
		InWater = false;
		Crouch = false;
		Run = false;
		Grounded = false;
		Cam->LookAt+=YVec3f(0, 0, Height);
		Cam->Position += YVec3f(0, 0, Height);
	}

	void update(float elapsed)
	{
		updateMouvement(elapsed);
	}

	void updateMouvement(float elapsed) {
		if (elapsed > 1.0f / 60.0f)
			elapsed = 1.0f / 60.0f;

		//Calcule les inputs
		YVec3f inputs = { 0,0,0 };
		YVec3f flatDirection = Cam->Direction;
		flatDirection.Z = 0;
		flatDirection.normalize();
		if (gauche) inputs -= Cam->RightVec;
		if (droite) inputs += Cam->RightVec;
		if (avance) inputs += flatDirection;
		if (recule) inputs -= flatDirection;
		inputs = inputs.normalize() * vitesse;

		//Calcule la vitesse
		Speed.Z = Speed.Z + gravity * elapsed;
		Speed.X = inputs.X;
		Speed.Y = inputs.Y;

		//Jump
		if (Jump && Grounded) {
			Speed.Z -= gravity;
			Jump = false;
		}
		Grounded = false;
		Jump = false;

		//calcule la position finale
		YVec3f deltaPos = Speed * elapsed;

		//Verifie la collision
		float valueColMin;
		Moove(deltaPos);
		MWorld::MAxis axis;
		do {
			axis = 0x00;
			axis = World->getMinCol(Position, deltaPos, Width, Height, valueColMin, true);
			if (axis == MWorld::AXIS_X) {
				Speed.X = 0;
				Moove({ valueColMin * 1.1f, 0, 0 });
			}
			if (axis == MWorld::AXIS_Y) {
				Speed.Y = 0;
				Moove({ 0, valueColMin * 1.1f, 0 });
			}
			if (axis == MWorld::AXIS_Z) {
				Speed.Z = 0;
				Moove({ 0, 0, valueColMin * 1.1f });
				Grounded = true;
			}
		} while (axis != 0x00);
		do {
			axis = 0x00;
			axis = World->getMinCol(Position, deltaPos, Width, Height, valueColMin, false);
			if (axis == MWorld::AXIS_X) {
				Speed.X = 0;
				Moove({ valueColMin * 1.1f, 0, 0 });
			}
			if (axis == MWorld::AXIS_Y) {
				Speed.Y = 0;
				Moove({ 0, valueColMin * 1.1f, 0 });
			}
			if (axis == MWorld::AXIS_Z) {
				Speed.Z = 0;
				Moove({ 0, 0, valueColMin * 1.1f });
				Grounded = true;
			}
		} while (axis != 0x00);
	}

	void Moove(YVec3f delta) {
		Position += delta;
		Cam->move( delta );
	}
};

#endif