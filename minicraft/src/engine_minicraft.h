#ifndef __YOCTO__ENGINE_TEST__
#define __YOCTO__ENGINE_TEST__

#include "engine/engine.h"

#include "avatar.h"
#include "world.h"

#include <iomanip>

class MEngineMinicraft : public YEngine {

public :

	//Gestion singleton
	static YEngine * getInstance()
	{
		if (Instance == NULL)
			Instance = new MEngineMinicraft();
		return Instance;
	}

	//fbo
	YFbo* Fbo;
	YFbo* FboShadow;
	YFbo* FboPostProcess;
	YFbo* FboBloom;
	GLuint PostProcessProg;

	//SUN
	GLuint ShaderCubeDebug;
	GLuint ShaderSun;
	GLuint ShaderShadow;
	YVbo* VboCube;
	YColor SunColor = { 1, 0.7f, 0.7f, 1 };
	YVec3f SunPosition;
	YVec3f SunDirection;
	float SunAngleD;
	time_t currentTime;
	struct tm* localTime;

	YColor skyColor;
	YTexFile* worldTex;
	YTexFile* hatchTex;
	const int SUNRISE_HOUR = 6 * 3600;
	const int SUNSET_HOUR = 19 * 3600;
	YColor SUNRISE_SUNSET_COLOR = {1, 0.73f , 0.5f, 1};
	int ZENITH_HOUR;
	YColor ZENITH_COLOR = {0.53f, 0.81f, 0.92f, 1};
	int NADIR_HOUR;
	YColor NADIR_COLOR = {0.01f, 0, 0.08f, 1};
	int hourOffset = 0;

	//MONDE
	MWorld* world;
	GLuint ShaderCube;
	GLuint ShaderWorld;

	//AVATAR
	MAvatar* Avatar;

	/*HANDLERS GENERAUX*/
	void loadShaders() {
		ShaderCubeDebug = Renderer->createProgram("shaders/cube_debug");
		ShaderSun = Renderer->createProgram("shaders/sun");
		ShaderCube = Renderer->createProgram("shaders/cube");
		ShaderWorld = Renderer->createProgram("shaders/world");
		ShaderShadow = Renderer->createProgram("shaders/shadow");
		PostProcessProg = Renderer->createProgram("shaders/postprocess");
	}

	void init() 
	{
		Fbo = new YFbo();
		Fbo->init(Renderer->ScreenWidth, Renderer->ScreenHeight);
		FboShadow = new YFbo();
		FboShadow->init(Renderer->ScreenWidth, Renderer->ScreenHeight);
		FboBloom = new YFbo();
		FboBloom->init(Renderer->ScreenWidth, Renderer->ScreenHeight);
		FboPostProcess = new YFbo();
		FboPostProcess->init(Renderer->ScreenWidth, Renderer->ScreenHeight);

		//init le zenith et le nadir
		ZENITH_HOUR = (SUNRISE_HOUR + (SUNSET_HOUR - SUNRISE_HOUR) / 2);
		NADIR_HOUR = (SUNSET_HOUR + (SUNRISE_HOUR+(24*3600) - SUNSET_HOUR) / 2);

		//init le world
		world = new MWorld();
		world->init_world(0);

		//init
		YLog::log(YLog::ENGINE_INFO,"Minicraft Started : initialisation");

		Renderer->setBackgroundColor(YColor(0.0f,0.0f,0.0f,1.0f));
		Renderer->Camera->setPosition(YVec3f(200, 200, 200));
		Renderer->Camera->setLookAt(YVec3f());

		//Init l'avatar
		Avatar = new MAvatar(Renderer->Camera, world);

		//Creation du VBO
		VboCube = new YVbo(3, 36, YVbo::PACK_BY_ELEMENT_TYPE);

		//D�finition du contenu du VBO
		VboCube->setElementDescription(0, YVbo::Element(3)); //Sommet
		VboCube->setElementDescription(1, YVbo::Element(3)); //Normale
		VboCube->setElementDescription(2, YVbo::Element(2)); //UV

		//On demande d'allouer la m�moire cot� CPU
		VboCube->createVboCpu();

		//D�finit les points
		YVec3f A = { 0,0,0 };
		YVec3f B = { 1,0,0 };
		YVec3f C = { 1,1,0 };
		YVec3f D = { 0,1,0 };
		YVec3f E = { 0,0,1 };
		YVec3f F = { 1,0,1 };
		YVec3f G = { 1,1,1 };
		YVec3f H = { 0,1,1 };

		//On ajoute les sommets
		int iVertice = 0;
		AddFace(iVertice, A, B, F, E, { 0,-1,0 });
		AddFace(iVertice, B, A, D, C, { 0,0,-1 });
		AddFace(iVertice, C, D, H, G, { 0,1,0 });
		AddFace(iVertice, E, F, G, H, { 0,0,1 });

		AddFace(iVertice, B, C, G, F, { 1,0,0 });
		AddFace(iVertice, D, A, E, H, { -1,0,0 });

		//On envoie le contenu au GPU
		VboCube->createVboGpu();

		//On relache la m�moire CPU
		VboCube->deleteVboCpu();

		worldTex = YTexManager::getInstance()->loadTexture("textures/world.png");
		hatchTex = YTexManager::getInstance()->loadTexture("textures/hatching.png");
	}

	void AddFace(int& iVertice, const YVec3f& vA, const YVec3f& vB, const YVec3f& vC, const YVec3f& vD, YVec3f normal) {
		VboCube->setElementValue(0, iVertice, vA.X, vA.Y, vA.Z); //Sommet (li� au layout(0) du shader)
		VboCube->setElementValue(1, iVertice, normal.X, normal.Y, normal.Z);   //Normale (li� au layout(1) du shader)
		VboCube->setElementValue(2, iVertice, 0, 0);      //UV (li� au layout(2) du shader)
		iVertice++;

		VboCube->setElementValue(0, iVertice, vB.X, vB.Y, vB.Z); //Sommet (li� au layout(0) du shader)
		VboCube->setElementValue(1, iVertice, normal.X, normal.Y, normal.Z);   //Normale (li� au layout(1) du shader)
		VboCube->setElementValue(2, iVertice, 1, 0);      //UV (li� au layout(2) du shader)
		iVertice++;

		VboCube->setElementValue(0, iVertice, vC.X, vC.Y, vC.Z); //Sommet (li� au layout(0) du shader)
		VboCube->setElementValue(1, iVertice, normal.X, normal.Y, normal.Z);   //Normale (li� au layout(1) du shader)
		VboCube->setElementValue(2, iVertice, 1, 1);      //UV (li� au layout(2) du shader)
		iVertice++;



		VboCube->setElementValue(0, iVertice, vA.X, vA.Y, vA.Z); //Sommet (li� au layout(0) du shader)
		VboCube->setElementValue(1, iVertice, normal.X, normal.Y, normal.Z);   //Normale (li� au layout(1) du shader)
		VboCube->setElementValue(2, iVertice, 0, 0);      //UV (li� au layout(2) du shader)
		iVertice++;

		VboCube->setElementValue(0, iVertice, vC.X, vC.Y, vC.Z); //Sommet (li� au layout(0) du shader)
		VboCube->setElementValue(1, iVertice, normal.X, normal.Y, normal.Z);   //Normale (li� au layout(1) du shader)
		VboCube->setElementValue(2, iVertice, 1, 1);      //UV (li� au layout(2) du shader)
		iVertice++;

		VboCube->setElementValue(0, iVertice, vD.X, vD.Y, vD.Z); //Sommet (li� au layout(0) du shader)
		VboCube->setElementValue(1, iVertice, normal.X, normal.Y, normal.Z);   //Normale (li� au layout(1) du shader)
		VboCube->setElementValue(2, iVertice, 0, 1);      //UV (li� au layout(2) du shader)
		iVertice++;
	}

	void update(float elapsed) 
	{
		updateSun();
		Avatar->update(elapsed);
	}

	void renderObjects()
	{
		glPushMatrix();
		//shadow map
		FboShadow->setAsOutFBO(true);
		glUseProgram(ShaderShadow);
		glDisable(GL_LIGHTING);
		YCamera* cam = Renderer->Camera;
		cam->setProjectionOrtho(-200, 200, -200, 200, cam->Near, cam->Far);
		cam->updateVecs();

		YVec3<float> previousPosition = cam->Position;
		cam->setPosition(SunPosition);
		YVec3<float> previousLookAt = cam->LookAt;
		cam->setLookAt(YVec3f(MWorld::MAT_SIZE_METERS/2, MWorld::MAT_SIZE_METERS/2, MWorld::MAT_HEIGHT_METERS/2));
		cam->look();
		cam->updateVecs();
		
		Renderer->updateMatricesFromOgl();
		Renderer->sendMatricesToShader(ShaderShadow);
		YMat44 shadowMVP = Renderer->getMVPMat();
		world->render_world_vbo(true, true, ShaderShadow);
		FboShadow->setAsOutFBO(false);
		glPopMatrix();
		cam->setPosition(previousPosition);
		cam->setLookAt(previousLookAt);
		cam->setProjectionPerspective(45.0f, 1920.0f / 1080.0f, 0.1f, 800.0f);
		cam->updateVecs();
		cam->look();
		
		FboBloom->setAsOutFBO(true);
		glUseProgram(ShaderSun);
		GLuint sun_colorSun = glGetUniformLocation(ShaderSun, "sun_color");
		glUniform3f(sun_colorSun, SunColor.R, SunColor.V, SunColor.B);
		renderSun();
		FboBloom->setAsOutFBO(false);
		//recuperer la map, la blur, et l'envoyer au shader world

		Fbo->setAsOutFBO(true);
		glUseProgram(0);
		//Rendu des axes
		glDisable(GL_LIGHTING);
		glBegin(GL_LINES);
		glColor3d(1, 0, 0);
		glVertex3d(0, 0, 0);
		glVertex3d(10000, 0, 0);
		glColor3d(0, 1, 0);
		glVertex3d(0, 0, 0);
		glVertex3d(0, 10000, 0);
		glColor3d(0, 0, 1);
		glVertex3d(0, 0, 0);
		glVertex3d(0, 0, 10000);
		glEnd();		

		//dessine le monde
		glUseProgram(ShaderWorld);
		GLuint sun_pos = glGetUniformLocation(ShaderWorld, "sun_pos");
		glUniform3f(sun_pos, SunPosition.X, SunPosition.Y, SunPosition.Z);
		GLuint sun_color = glGetUniformLocation(ShaderWorld, "sun_color");
		glUniform3f(sun_color, SunColor.R, SunColor.V, SunColor.B);
		GLuint cam_pos = glGetUniformLocation(ShaderWorld, "cam_pos");
		glUniform3f(cam_pos,Avatar->Cam->Position.X, Avatar->Cam->Position.Y, Avatar->Cam->Position.Z);
		worldTex->setAsShaderInput(ShaderWorld, GL_TEXTURE0, "worldTex");
		hatchTex->setAsShaderInput(ShaderWorld, GL_TEXTURE1, "hatching");
		FboShadow->setDepthAsShaderInput(GL_TEXTURE2, "texDepth");
		Renderer->sendScreenSizeToShader(ShaderWorld);
		Renderer->sendNearFarToShader(ShaderWorld);
		GLuint smvp = glGetUniformLocation(ShaderWorld, "smvp");
		glUniformMatrix4fv(smvp, 1, true, shadowMVP.Mat.t);
		world->render_world_vbo(true, true, ShaderWorld);

		glUseProgram(ShaderSun);
		glUniform3f(sun_colorSun, SunColor.R, SunColor.V, SunColor.B);
		renderSun();

		Renderer->setBackgroundColor(skyColor); //change la couleur du ciel

		Fbo->setAsOutFBO(false);

		glUseProgram(PostProcessProg);

		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);

		Fbo->setColorAsShaderInput(0, GL_TEXTURE0, "TexColor");
		Fbo->setDepthAsShaderInput(GL_TEXTURE1, "TexDepth");
		FboBloom->setColorAsShaderInput(0, GL_TEXTURE2, "BloomColor");
		FboBloom->setDepthAsShaderInput(GL_TEXTURE3, "BloomDepth");


		Renderer->sendNearFarToShader(PostProcessProg);
		Renderer->sendScreenSizeToShader(PostProcessProg);
		Renderer->drawFullScreenQuad();

	}

	void updateSun() {
		struct tm newtime;
		long ltime;
		_time32(&ltime);

		_gmtime32_s(&newtime, &ltime);

		int hour = newtime.tm_hour;
		int min = newtime.tm_min;
		int sec = newtime.tm_sec;

		float timeInSeconds = sec + min * 60 + ((hour + hourOffset) % 24) * 3600;

		if (timeInSeconds < SUNRISE_HOUR) {
			skyColor = NADIR_COLOR.interpolate(SUNRISE_SUNSET_COLOR, timeInSeconds / float(SUNRISE_HOUR));
		}
		else if (timeInSeconds < ZENITH_HOUR) {
			skyColor = SUNRISE_SUNSET_COLOR.interpolate(ZENITH_COLOR, (timeInSeconds - SUNRISE_HOUR) / float(ZENITH_HOUR - SUNRISE_HOUR));
		}
		else if (timeInSeconds < SUNSET_HOUR) {
			skyColor = ZENITH_COLOR.interpolate(SUNRISE_SUNSET_COLOR, (timeInSeconds - ZENITH_HOUR) / float(SUNSET_HOUR - ZENITH_HOUR));
		}
		else {
			skyColor = SUNRISE_SUNSET_COLOR.interpolate(NADIR_COLOR, (timeInSeconds - SUNSET_HOUR) / float(NADIR_HOUR - SUNSET_HOUR));
		}

		SunAngleD = ((timeInSeconds - SUNSET_HOUR) / (24.f * 3600.f)) * 360 + 180;

		YVec3f SunDirection = { (float)cos(SunAngleD / 180 * M_PI), 0, (float)sin(SunAngleD / 180 * M_PI) };
		SunPosition = YVec3f(MWorld::MAT_SIZE_METERS/2, MWorld::MAT_SIZE_METERS / 2, MWorld::MAT_HEIGHT_METERS / 2) + SunDirection * 700;
	}

	void renderSun() {
		glPushMatrix();
		glUseProgram(ShaderSun);
		glTranslatef(SunPosition.X, SunPosition.Y, SunPosition.Z);
		glScalef(40, 40, 40);
		Renderer->updateMatricesFromOgl();
		Renderer->sendMatricesToShader(ShaderSun);
		VboCube->render();
		glPopMatrix();
	}

	void resize(int width, int height) {
		Fbo->resize(Renderer->ScreenWidth, Renderer->ScreenHeight);
		FboPostProcess->resize(Renderer->ScreenWidth, Renderer->ScreenHeight);
		FboShadow->resize(Renderer->ScreenWidth, Renderer->ScreenHeight);
		FboBloom->resize(Renderer->ScreenWidth, Renderer->ScreenHeight);
	}

	/*INPUTS*/

	void keyPressed(int key, bool special, bool down, int p1, int p2) 
	{	
		if (key == 'g' && down) {
			hourOffset++;
		}
		if (key == 'z' ) {
			Avatar->avance = down;
		}
		if (key == 'q') {
			Avatar->gauche = down;
		}
		if (key == 's' ) {
			Avatar->recule = down;
		}
		if (key == 'd') {
			Avatar->droite = down;
		}
		if (key == 'e' && !down) {

			YVec3f inter = { 0,0,0 };
			int x, y, z;
			if (world->getRayCollision(Avatar->Cam->Position, Avatar->Cam->Position + Avatar->Cam->Direction * 10, inter, x, y, z)) {
				glPushMatrix();
				glUseProgram(0);
				YRenderer::getInstance()->updateMatricesFromOgl(); //Calcule toute les matrices � partir des deux matrices OGL
				YRenderer::getInstance()->sendMatricesToShader(0); //Envoie les matrices au shader
				glPopMatrix();
				world->deleteCube(x, y, z);
			}
		}
		if (key == VK_SPACE && down) {
			Avatar->Jump = true;
		}
	}

	void mouseClick(int button, int state, int x, int y, bool inUi)
	{

	}

	void mouseMove(int x, int y, bool pressed, bool inUi)
	{
		static int lastx = -1;
		static int lasty = -1;

		if (!pressed)
		{
			lastx = x;
			lasty = y;
			showMouse(true);
		}
		else
		{
			if (lastx == -1 && lasty == -1)
			{
				lastx = x;
				lasty = y;
			}

			int dx = x - lastx;
			int dy = y - lasty;

			if (dx == 0 && dy == 0)
				return;

			lastx = x;
			lasty = y;

			if (MouseBtnState & GUI_MRBUTTON)
			{
				showMouse(false);
				if (GetKeyState(VK_LCONTROL) & 0x80)
				{
					Renderer->Camera->rotateAround((float)-dx / 300.0f);
					Renderer->Camera->rotateUpAround((float)-dy / 300.0f);
					glutWarpPointer(Renderer->ScreenWidth / 2, Renderer->ScreenHeight / 2);
					lastx = Renderer->ScreenWidth / 2;
					lasty = Renderer->ScreenHeight / 2;
				}
				else {
					showMouse(false);
					Renderer->Camera->rotate((float)-dx / 300.0f);
					Renderer->Camera->rotateUp((float)-dy / 300.0f);
					glutWarpPointer(Renderer->ScreenWidth / 2, Renderer->ScreenHeight / 2);
					lastx = Renderer->ScreenWidth / 2;
					lasty = Renderer->ScreenHeight / 2;
				}
			}

			if (MouseBtnState & GUI_MMBUTTON)
			{
				showMouse(false);
				if (GetKeyState(VK_LCONTROL) & 0x80)
				{
					YVec3f strafe = Renderer->Camera->RightVec;
					strafe.Z = 0;
					strafe.normalize();
					strafe *= (float)-dx / 2.0f;

					YVec3f avance = Renderer->Camera->Direction;
					avance.Z = 0;
					avance.normalize();
					avance *= (float)dy / 2.0f;

					Renderer->Camera->move(avance + strafe);
				}
				else {
					YVec3f strafe = Renderer->Camera->RightVec;
					strafe.Z = 0;
					strafe.normalize();
					strafe *= (float)-dx / 5.0f;

					Renderer->Camera->move(Renderer->Camera->UpRef * (float)dy / 5.0f);
					Renderer->Camera->move(strafe);
					glutWarpPointer(Renderer->ScreenWidth / 2, Renderer->ScreenHeight / 2);
					lastx = Renderer->ScreenWidth / 2;
					lasty = Renderer->ScreenHeight / 2;
				}
			}
		}
	}

	void mouseWheel(int wheel, int dir, int x, int y, bool inUi)
	{
		Renderer->Camera->move(Renderer->Camera->Direction * 10.0f * dir);
	}
	
};


#endif