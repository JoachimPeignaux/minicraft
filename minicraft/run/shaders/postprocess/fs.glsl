#version 400

in vec2 uv;

uniform sampler2D TexColor;
uniform sampler2D TexDepth;
uniform sampler2D BloomColor;
uniform sampler2D BloomDepth;
uniform float screen_width;
uniform float screen_height;
uniform vec2 near_far;

out vec4 color_out;

float LinearizeDepth(float z)
{
	float n = near_far.x; // camera z near
  	float f = near_far.y; // camera z far
  	return (2.0 * n) / (f + n - z * (f - n));
}

const int blurIteration = 5;

void main (void)
{
	float xstep = 1.0/screen_width;
	float ystep = 1.0/screen_height;
	float ratio = screen_width / screen_height;

	vec4 color = texture2D( TexColor , uv );
	float depth = texture2D( TexDepth , uv ).r;	
	
	//Permet de scaler la profondeur
	depth = LinearizeDepth(depth);

	//devrai faire de chromatique
	vec2 toCenter = vec2(0.5, 0.5) - uv;
	float distCenter = length(toCenter);
	vec4 colorXp = texture2D(TexColor, uv - toCenter * 10 * xstep);
	//color.r = colorXp.r;

	//flouter le bloom
	vec3 bloomCol = texture(BloomColor, uv).rgb;
	vec3 bloomDepth = texture(BloomDepth, uv).rgb;
	float sizeFlou = 20;
	int c=0;
	for( int x=-blurIteration/2; x<=blurIteration/2; x++){
		for( int y=-blurIteration/2; y<=blurIteration/2; y++){
			if(abs(x)+abs(y) <= blurIteration)
			{
				bloomCol += texture( BloomColor, uv + vec2(x*xstep,y*ystep)*sizeFlou).rgb;
				bloomDepth += texture( BloomDepth, uv + vec2(x*xstep,y*ystep)*sizeFlou).rgb;
				c++;
			}
		}
	}
	bloomCol /= (1 + c);
	bloomDepth = min(bloomDepth, 1.0);
	/*float centerDepth = texture2D(TexDepth, vec2(0.5, 0.5)).r;
	centerDepth = LinearizeDepth(centerDepth);
	color.rgb = mix(color.rgb, bloomCol.rgb, abs(centerDepth-depth));*/


	/*vec3 colorEdge = color.rgb*4;
	float sizeEdge = 3;
	colorEdge -= texture2D( TexColor, uv + vec2(xstep,0)*sizeEdge).rgb;
	colorEdge -= texture2D( TexColor, uv + vec2(-xstep,0)*sizeEdge).rgb;
	colorEdge -= texture2D( TexColor, uv + vec2(0, ystep)*sizeEdge).rgb;
	colorEdge -= texture2D( TexColor, uv + vec2(0, -ystep)*sizeEdge).rgb;
	float edgeOnColor = length(colorEdge.rgb); 

	float depthEdge = 4 * depth;
	depthEdge -= LinearizeDepth(texture2D( TexDepth, uv + vec2(xstep,0)*sizeEdge).r);
	depthEdge -= LinearizeDepth(texture2D( TexDepth, uv + vec2(-xstep,0)*sizeEdge).r);
	depthEdge -= LinearizeDepth(texture2D( TexDepth, uv + vec2(0, ystep)*sizeEdge).r);
	depthEdge -= LinearizeDepth(texture2D( TexDepth, uv + vec2(0, -ystep)*sizeEdge).r);
	float edgeOnDepth = clamp(abs(depthEdge) * 50, 0, 1);

	color.rgb += edgeOnColor * vec3(0,0,0) + edgeOnDepth * vec3(1,1,1);*/
	
	//float lumi = 0.2* color.r + 0.7*color.g + 0.1*color.b;


    //Gamma correction
    color.r = pow(color.r,1.0/2.2);
    color.g = pow(color.g,1.0/2.2);
    color.b = pow(color.b,1.0/2.2);

	vec2 posInText = vec2(gl_FragCoord.x / screen_width, gl_FragCoord.y / screen_height);

	if(texture(BloomDepth, uv).z == 1.0 && texture(TexDepth, uv).z == 1.0) //1 if there is nothing
	{
		color_out = vec4(bloomCol.rgb, 1.0);
	}
	else
	{
		color_out = vec4(color.rgb,1.0);
	}

	//merge de couleur
	color.rgb = mix(color.rgb, vec3(0.2,0.2,0.2), pow(depth, 0.3));

}