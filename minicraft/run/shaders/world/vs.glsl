#version 400

//uniforms
uniform float elapsed;
	//regular pos
uniform mat4 m;
uniform mat4 v;
uniform mat4 p;
uniform mat4 nmat;
	//shadow pos
uniform mat4 smvp;
const mat4x4 biasMatrix = mat4x4
(
0.5, 0.0, 0.0, 0.0,
0.0, 0.5, 0.0, 0.0,
0.0, 0.0, 0.5, 0.0,
0.5, 0.5, 0.5, 1.0
);

//entree
layout(location=0) in vec3 vs_position_in;
layout(location=1) in vec3 vs_normal_in;
layout(location=2) in vec2 vs_uv_in;
layout(location=3) in float vs_type_in;

//Variables en sortie
out vec3 normal;
out vec4 color;
out vec2 uv;
out vec4 wPos;
out float type_cube;
out vec4 shadowPos;

//types
#define CUBE_HERBE 0.0
#define CUBE_TERRE 1.0
#define CUBE_PIERRE 3.0
#define CUBE_EAU 4.0
#define CUBE_SABLE_01 17.0

float noiseWater(vec2 pos){
	float delta = cos(pos.x /3 + elapsed)*2/3;
	delta += cos(pos.y + elapsed*1.5)*1/3;
	return delta;
}

void main()
{

	vec4 vecIn = vec4(vs_position_in,1.0);
	wPos = m * vecIn;

	if(vs_type_in == CUBE_EAU) { //waves
		wPos.z += noiseWater(wPos.xy)/2-1;
	}

	gl_Position = wPos;
	shadowPos = biasMatrix * (smvp * vecIn);
		
	normal = (nmat * vec4(vs_normal_in,1.0)).xyz; 
	normal = normalize(normal);

	uv = vs_uv_in;

	//Couleur par défaut violet
	color = vec4(1.0,1.0,0.0,1.0);

	//Couleur fonction du type
	if(vs_type_in == CUBE_HERBE)
		color = vec4(0,1,0,1);
	if(vs_type_in == CUBE_TERRE)
		color = vec4(0.2,0.1,0,1);
	if(vs_type_in == CUBE_EAU)
		color = vec4(0.0,0.0,1.0,0.7);
	if(vs_type_in == CUBE_SABLE_01)
		color = vec4(1,1,0,1);
	if (vs_type_in == CUBE_PIERRE)
		color = vec4(0.2, 0.2, 0.2, 1);

	type_cube = vs_type_in;
}