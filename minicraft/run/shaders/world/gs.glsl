#version 400

//uniforms
uniform float elapsed;
uniform mat4 v;
uniform mat4 p;
uniform vec3 cam_pos;

//entree
layout (triangles, invocations = 2) in;
layout (triangle_strip, max_vertices = 18) out;
in vec3 normal[];
in vec4 color[];
in vec2 uv[];
in vec4 wPos[];
in float type_cube[];
in vec4 shadowPos[];

//sortie
out vec3 normal_fs;
out vec4 color_fs;
out vec2 uv_fs;
out vec4 wPos_fs;
out float type_cube_fs;
out vec4 shadowPos_fs;

//Types
#define CUBE_HERBE 0.0
#define CUBE_TERRE 1.0
#define CUBE_PIERRE 3.0
#define CUBE_EAU 4.0
#define CUBE_SABLE_01 17.0

//globales
const vec4 GREEN = vec4(0.7f, 1.f, 0.54f, 1.f);

float rand01(vec2 co){
    return (sin(dot(co, vec2(12.9898, 78.233)) * 43758.5453) + 1) / 2.0f;
}

void drawBladeVertex(vec4 position, vec4 shadowPos, vec3 triNormal) {
    gl_Position = position;
    shadowPos_fs = shadowPos;
    normal_fs = triNormal;
    color_fs = GREEN;
    uv_fs = uv[0];
    wPos_fs = position;
    type_cube_fs = -1;
    EmitVertex();
}

void main()
{	

  int i;
  
  //rendre le triangle de base
  for(i=0; i<3; i++)
  {
      vec4 vecIn = p * ( v * vec4(gl_in[i].gl_Position));
      gl_Position = vecIn;
      normal_fs = normal[i];
      color_fs = color[i];
      uv_fs = uv[i];
      wPos_fs = wPos[i];
      shadowPos_fs = shadowPos[i];
      type_cube_fs = type_cube[i];
      EmitVertex();
  }
  EndPrimitive();
  
  //rajouter lherbe
  vec3 a = wPos[0].xyz - wPos[1].xyz;
  vec3 b = wPos[2].xyz - wPos[1].xyz;
  vec3 triNormal = normalize(cross(b, a));
  
  if(type_cube[0] == CUBE_HERBE && triNormal == vec3(0,0,1))
  {
      vec3 middle = (vec3(wPos[0]) + vec3(wPos[1]) + vec3(wPos[2]))/3;

      if(distance(middle, cam_pos) < 80) //ne dessine pas l'herbe apres 80m
      {
          for(i=0; i<3; i++)
          {
              float r1 = rand01(wPos[0].xy+i);
              float r2 = rand01(wPos[0].xy+i+1);
              vec3 left = (wPos[1].xyz + a * r1);
              vec3 center = left + ( (wPos[1].xyz + b * r1) - left )*r2;
              vec3 direction = normalize(center - wPos[int(floor(mod(i,3)))].xyz);

              vec4 bladeL = p * ( v * vec4(center.xyz - direction*0.1, 1.0));
              vec4 bladeR = p * ( v * vec4(center.xyz + direction*0.1, 1.0));

              vec3 topMovement;
              vec3 playerFeet = vec3(cam_pos.xy, middle.z);

              //deplace le haut de l'herbe
              if(distance(middle, playerFeet) < 2)
              {
                    vec3 camDirection = middle - playerFeet;
                    topMovement = normalize(camDirection)*0.7;
              } else {
                    topMovement = vec3(abs(cos(elapsed/3-center.x/5)), 0, -abs(cos(elapsed/3-center.x/5))/2);
              }

              vec4 bladeT = p * ( v * vec4(center.xyz + triNormal.xyz + topMovement.xyz, 1.0));
              vec3 bladeNormal = cross(bladeT.xyz - bladeL.xyz, bladeT.xyz- bladeR.xyz);
              vec3 camDir = (center - cam_pos);

              //choisi l'ordre pour etre toujours visible
              if(dot(camDir, bladeNormal) < 0) {
                  drawBladeVertex(bladeL, shadowPos[0], triNormal); 
                  drawBladeVertex(bladeR, shadowPos[0], triNormal); 
              } else {
                  drawBladeVertex(bladeR, shadowPos[0], triNormal); 
                  drawBladeVertex(bladeL, shadowPos[0], triNormal); 
              }
              drawBladeVertex(bladeT, shadowPos[0], triNormal); 

              EndPrimitive();
          }
      }
  }
}