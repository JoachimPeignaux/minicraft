#version 400

//Variables en entree
in vec3 normal_fs;
in vec4 color_fs;
in vec2 uv_fs;
in vec4 wPos_fs;
in float type_cube_fs;
in vec4 shadowPos_fs;

//uniform
uniform float elapsed;
uniform vec3 sun_pos;
uniform vec3 cam_pos;
uniform vec3 sun_color;
uniform sampler2D worldTex;
uniform sampler2D hatching;
uniform sampler2D texDepth;
uniform float screen_width;
uniform float screen_height;
uniform vec2 near_far;

//sortie
out vec4 color_out;

//Globales
const float ambientLevel = 0.4;

//Types
#define CUBE_HERBE 0.0
#define CUBE_TERRE 1.0
#define CUBE_PIERRE 3.0
#define CUBE_EAU 4.0
#define CUBE_SABLE_01 17.0

float noiseWater(vec2 pos){
	float delta = cos(pos.x /3 + elapsed)*2/3;
	delta += cos(pos.y + elapsed*1.5)*1/3;
	return delta;
}

float LinearizeDepth(float z)
{
	float n = near_far.x; // camera z near
  	float f = near_far.y; // camera z far
  	return (2.0 * n) / (f + n - z * (f - n));
}

void main()
{
	vec3 normalOK = normal_fs;
	vec3 lightDir = normalize(sun_pos.xyz - wPos_fs.xyz);

	int uvXOffset = -1;

	if(type_cube_fs == CUBE_EAU){
		vec3 A = wPos_fs.xyz;
		vec3 B = A + vec3(0.01, 0, 0);
		vec3 C = A + vec3(0, 0.01, 0);

		A.z+= noiseWater(A.xy);
		B.z += noiseWater(B.xy);
		C.z += noiseWater(C.xy);
		normalOK = normalize(cross((B-A),(C-A)));
	} else if (type_cube_fs == CUBE_PIERRE)
	{
		uvXOffset = 3;
	}else if (type_cube_fs == CUBE_SABLE_01)
	{
		uvXOffset = 20;
	}else if (type_cube_fs == CUBE_TERRE)
	{
		uvXOffset = 1;
	} else if (type_cube_fs == CUBE_HERBE)
	{
		uvXOffset = 0;
	}

	float diffuse = 0;
	float specular = 0;

	
	if( LinearizeDepth(texture(texDepth, shadowPos_fs.xy).z) > LinearizeDepth(shadowPos_fs.z - 0.005) )
	{
		diffuse = max(0.0,dot(lightDir, normalOK));
		vec3 viewDir = normalize(cam_pos - wPos_fs.xyz);
		vec3 halfVec = normalize(lightDir + viewDir);
		specular = max(0.0,dot(normalOK, halfVec));
		specular = pow(specular, 100);
	}
	
	//hatching
	//float numTex = 1-(floor((diffuse+specular) * 5)/5);
	//vec2 uv_fsHatch = uv_fs;
	//uvHatch.x+=numTex;
	//uvHatch.x/=5;
	
	//colorTex.rgb *= texture2D(hatching, uv_fsHatch).rgb;
	

	if(uvXOffset == -1){
		color_out.xyz = max(0.1,diffuse) * color_fs.xyz + specular * vec3(1,1,1);
	} else {
		vec2 uvOk = uv_fs;
		uvOk.x += uvXOffset;
		uvOk.y +=1;
		uvOk.y -= normal_fs.z;
		uvOk.x/=32;
		uvOk.y/=2;
		vec4 colorTex = texture2D(worldTex, uvOk);
		color_out.xyz = max(0.1,diffuse) * colorTex.xyz + specular * vec3(1,1,1);
	}
	
	color_out.a = color_fs.a;
}